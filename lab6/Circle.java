public class Circle {

    static  final double Pi =3.14;
    int radius;
    Point centerPoint;

    public Circle(int radius, Point point) {
        this.radius = radius;
        this.centerPoint = point;
    }

    public double area() {
        return Pi * radius * radius;
    }

    public double perimeter() {
        return Pi * 2 * radius;
    }

    public boolean intersect(Circle circle) {
        return radius+ circle.radius >= centerPoint.distance(circle.centerPoint);
    }

}
