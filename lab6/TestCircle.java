public class TestCircle {
    public static void main(String[] args) {

        Circle c1= new Circle(8,new Point(15,20));

        System.out.println("Area of Circle : "+c1.area());
        System.out.println("Perimeter of Circle : "+c1.perimeter());

        Circle c2= new Circle(8,new Point(22,44));

        System.out.println(c2.intersect(c1));
    }
}
