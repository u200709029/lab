public class Point {
   private int xCordinate;
   private int yCordinate;

    public Point(int xCordinate, int yCordinate) {
        this.xCordinate = xCordinate;
        this.yCordinate = yCordinate;
    }

    public int getxCordinate() {
        return xCordinate;
    }

    public void setxCordinate(int xCordinate) {
        this.xCordinate = xCordinate;
    }

    public int getyCordinate() {
        return yCordinate;
    }

    public void setyCordinate(int yCordinate) {
        this.yCordinate = yCordinate;
    }

    public double distance(Point point){

        int distance= ((point.xCordinate-xCordinate)^2+(point.yCordinate-yCordinate)^2)^1/2;
        return distance;
    }
}
