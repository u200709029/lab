public class Rectangle {
    private Point topLeft;
    private int width;
    private int height;

    public Rectangle(int width, int height, Point topLeft ) {
        this.width = width;
        this.height = height;
        this.topLeft = topLeft;
    }

    public int findArea(){
        return width*height;
    }

    public int perimeter(){
        return 2*(width+height);
    }

    public Point[] corenrs(){
        Point [] corners = new Point[4];

        corners[0] = topLeft;
        corners[1] = new Point(topLeft.getxCordinate()+width, topLeft.getyCordinate());
        corners[2] = new Point(topLeft.getxCordinate(), topLeft.getyCordinate()-height);
        corners[3] = new Point(topLeft.getxCordinate()-width, topLeft.getyCordinate());


        return corners;
    }


}
