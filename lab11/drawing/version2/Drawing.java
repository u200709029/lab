package drawing.version2;

import java.util.ArrayList;

import shapes.Circle;
import shapes.Rectangle;
import shapes.Square;

public class Drawing {
	
	private ArrayList<Object> shapes = new ArrayList<Object>();
	
	public double calculateTotalArea(){
		double totalArea = 0;

		for (Object shape : shapes){
			if(shape instanceof Circle)  {
				Circle c = (Circle) shape;
				totalArea += c.area();
			}
			if(shape instanceof Rectangle)  {
				Rectangle r = (Rectangle) shape;
				totalArea += r.area();
			}
			if(shape instanceof Square)  {
				Square r = (Square) shape;
				totalArea += r.area();
			}

		}
		return totalArea;
	}
	
	public void addShape(Object c) {
		shapes.add(c);
	}

}
