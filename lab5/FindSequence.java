import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FindSequence {

	public static void main(String[] args) throws FileNotFoundException{

		System.out.println(search2(0,matrix(),2,4));
		printMatrix(matrix());

	}


	private static boolean search2 (int number, int[][]matrix, int row, int col) {
		if (row < 0 || row >= matrix.length || col<0 || col >= matrix[0].length )
			return false;

		boolean found;

		if (number == 9){
			return matrix[row][col] == number;
		}else{
			found = (matrix[row][col] == number)  && (
					search2(number +1, matrix, row+1,col) ||
							search2(number +1, matrix, row-1,col) ||
							search2(number +1, matrix, row,col+1) ||
							search2(number +1, matrix, row,col-1) );
		}


		if (found){
			matrix[row][col]=9-matrix[row][col];
		}
		return found;


	}


	private static void search (int currentNumber, int[][]matrix, int row, int col) {

		if (matrix[row][col+1]==currentNumber+1){
			System.out.println(matrix[row][col+1]+ "  Konum : "+row+", "+(col+1));
			search(currentNumber+1,matrix,row,col+1);
		}
		else if (matrix[row][col-1]==currentNumber+1){
			System.out.println(matrix[row][col-1]+ "  Konum : "+row+", "+(col-1));
			search(currentNumber+1,matrix,row,col-1);
		}
		else if (matrix[row+1][col]==currentNumber+1){
			System.out.println(matrix[row+1][col]+ "  Konum : "+(row+1)+", "+(col));
			search(currentNumber+1,matrix,row+1,col);
		}
		else if (matrix[row-1][col]==currentNumber+1){
			System.out.println(matrix[row][col-1]+ "  Konum : "+(row-1)+", "+(col));
			search(currentNumber+1,matrix,row-1,col);
		}else {
			System.out.println("bulunamadı");

		}

	}

	private static int[][] matrix() throws FileNotFoundException{
		int[][] matrix = new int[10][10];
		File file = new File("C:/Users/Enes/Desktop/matrix.txt"); // if you run from command line use  new File("matrix.txt") instead

		try (Scanner sc = new Scanner(file)){
			int i = 0;
			int j = 0;
			while (sc.hasNextLine()) {
				int number = sc.nextInt();
				matrix[i][j] = number;
				if (j == 9)
					i++;
				j = (j + 1) % 10;
				if (i == 10)
					break;
			}
		} catch (FileNotFoundException e) {
			throw e;
		}
		return matrix;
	}

	
	public static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
					System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}
	}





}
