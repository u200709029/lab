public class GCDLoop {
    public static void main(String[] args) {
//        System.out.println(GCD2(12,20));
        System.out.println(GCD4(12,9));
    }


    static int GCD4(int a, int b){  //Recursive
        int remainder = a%b;
        if (remainder == 0)
            return b;
        return GCD4(b, remainder);
    }


    static int GCD2(int a, int b){  //Recursive
        if (a == 0)
            return b;
        return GCD2(b%a, a);
    }


    static int GCD3(int number1, int number2){
        int remainder;

        do {
            remainder= number1%number2;
            if (remainder!=0){
                number1=number2;
                number2=remainder;
            }
        }while (remainder !=0);
            return number2;

    }

    static int GCD(int a, int b){
        int gcd=1;
        for(int i = 1; i <= (Math.min(a, b)); i++){
            if(a%i==0 && b%i==0){
                gcd=i;
            }
        }
        return gcd;
    }



}
