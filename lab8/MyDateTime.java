public class MyDateTime {

    MyDate date;

    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public String toString() {
        return date + " " + time;
        // return date.toString() + " " + time.toString();
    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementHour() {
        incrementHour(1);
    }

    public void incrementHour(int diff) {
        int dayDiff = time.incermentHour(diff);
        if (dayDiff<0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);
    }

    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff) {

        int dayDiff = time.incrementMinute(diff);
        if (dayDiff<0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);

    }

    public void decrementMinute(int diff) {
        incrementMinute(-diff);
    }

    public void incrementYear(int i) {
        date.incrementYear(i);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }
    public void incrementDay (int i) {
        date.incrementDay(i);
    }

    public void decrementMonth(int i) {
        date.decrementMonth(i);
    }

    public void decrementDay(int i) {
        date.decrementDay(i);
    }

    public void incrementMonth(int i) {
        date.incrementMonth(i);
    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public void decrementYear(int i) {
        date.decrementYear(i);
    }

    public void incrementYear() {
        date.incrementYear();
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        int date1 = Integer.parseInt((date.year+ date.month+ date.day+ time.hour+ time.minute+""));
        int date2 = Integer.parseInt((anotherDateTime.date.year+ anotherDateTime.date.month
                + anotherDateTime.date.day+ anotherDateTime.time.hour+ anotherDateTime.time.minute+""));

        return date1<date2;
    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        int date1 = Integer.parseInt((date.year+ date.month+ date.day+ time.hour+ time.minute+""));
        int date2 = Integer.parseInt((anotherDateTime.date.year+ anotherDateTime.date.month
                + anotherDateTime.date.day+ anotherDateTime.time.hour+ anotherDateTime.time.minute+""));

        return date2<date1;
    }

    public String dayTimeDifference(MyDateTime anotherDateTime) { // !!!!! I'AM WORKİNG ON IT...

        int dateThis = Integer.parseInt((date.month+ date.day+ time.hour+ time.minute+""));
        int dateAnother = Integer.parseInt((anotherDateTime.date.month + anotherDateTime.date.day+
                anotherDateTime.time.hour+ anotherDateTime.time.minute+""));

        if(this.isAfter(anotherDateTime)){
            MyDateTime mydatetime = new MyDateTime(new MyDate(date.day,date.month+1,date.year),new MyTime(time.hour,time.minute));
            while(mydatetime.date.isAfter(anotherDateTime.date)){
                anotherDateTime.date.incrementDay();
                System.out.println(" = ");
            }
        }
        if(this.isBefore(anotherDateTime)){
            MyDateTime mydatetime = new MyDateTime(new MyDate(date.day,date.month+1,date.year),new MyTime(time.hour,time.minute));
            while(mydatetime.date.isBefore(anotherDateTime.date)){
                mydatetime.date.incrementDay();
                System.out.println(mydatetime.date.month+" "+mydatetime.date.day);
            }
        }

        int minuteThis= time.hour*60+time.minute;
        int minuteAnother= anotherDateTime.time.hour*60+anotherDateTime.time.minute;

        if(minuteThis-minuteAnother>0){
            return (minuteThis-minuteAnother)/60+" hour "+(minuteThis-minuteAnother)%60+" minute";
        }else {
            return -(minuteThis-minuteAnother)/60+" hour "+ -(minuteThis-minuteAnother)%60+" minute";
        }

    }
}