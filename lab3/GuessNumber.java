import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuesNumber{
    public static void main(String[] args) {
        Scanner scanf = new Scanner(System.in);
        int guess;
        Random rand = new Random();
        int bound = 40;
        int number = rand.nextInt(bound);

        int counter;

        System.out.println("How many step do you need do play: ");
        counter = scanf.nextInt();

        while (counter > 0) {
            System.out.println("Enter a number: ");
            guess = scanf.nextInt();

        if (bound < guess) {
            System.out.println("Your guess must be lower than " + bound);
        }
        else {
            if (guess > number) {
                System.out.println("Lower.");
                counter -= 1;
                System.out.println(counter + " heart remain.");
            }
            if (guess < number) {
                System.out.println("Upper.");
                counter -= 1;
                System.out.println(counter + " heart remain.");
            }
            if (guess == number) {
                System.out.println("Congrulations.");
                break;

            }
        }
    }
    }

}