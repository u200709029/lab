public class FindMaximum {

	public static void main(String[] args){
		int value1 = Integer.parseInt(args[0]);
        int value2 = Integer.parseInt(args[1]);
        int result;


        result = value1 > value2 ? value1 : value2;

        System.out.println(result);

	}
}