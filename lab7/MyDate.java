public class MyDate {
    int day, month, year;

    int[] maxDays = {31,29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public MyDate(int day, int month, int year){
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String toString(){
        return year + "-"+ (month < 10 ? "0" : "")  + month + "-"+ (day<10 ? "0" : "")+ day;
    }

    public void incrementDay(){
        day++;
        if(day> maxDays[month-1]){
            day = 1;
            incrementMonth();
        }
        else if(month == 2 && day ==29 && !inLeapYear()){
            day = 1;
            incrementMonth();
        }
    }

    public boolean inLeapYear(){  //true = 28  false = 29
        return year % 4 == 0;
    }


    public void incrementYear(int diff){
        year += diff;
        if(month == 2 && day ==29 && !inLeapYear()){
            day = 28;
        }
    }


    public void decrementDay(){
        day--;
        if(day==0){
            decrementMonth();
            if (!inLeapYear() && month == 2){
                day = 28;
            }
            else {
                day = maxDays[month-1];

            }
        }
    }

    public void decrementYear(){
        incrementYear(-1);
    }

    public void decrementMonth(){
        decrementMonth(1);
    }

    public void incrementDay(int diff){
        while (diff > 0){
            incrementDay();
            diff--;
        }
    }

    public void decrementMonth(int diff){
        incrementMonth(-diff);
    }
    public void incrementMonth(int diff){
        month += diff;
        int yearDiff = (month-1) / 12;

        int newMonth = ((month-1) % 12) + 1;

        if(newMonth < 0)
            yearDiff--;
        year += yearDiff;

        month = newMonth<0 ? newMonth+12 : newMonth;

        if(day>maxDays[month-1]){
            day = maxDays[month-1];
            if(month == 2 && day ==29 && !inLeapYear()){
                day = 28;
            }

        }
    }

    public void decrementDay(int diff){
        while (diff>0){
            decrementDay();
            diff--;
        }

    }

    public void decrementYear(int diff){
        incrementYear(-diff);
    }

    public void incrementMonth(){
        incrementMonth(1);
    }

    public void incrementYear(){
        incrementYear(1);
    }

    public boolean isBefore(MyDate anotherDate) {
        boolean isbefore=false;

        isbefore = (year < anotherDate.year) ? true : isbefore;
        isbefore = (month < anotherDate.month) ? true : isbefore;
        isbefore = (day < anotherDate.day) ? true : isbefore;

        return isbefore;
    }

    public boolean isAfter(MyDate anotherDate) {
        boolean isafter=true;

        isafter = (year < anotherDate.year) ? false : isafter;
        isafter = (month < anotherDate.month) ? false : isafter;
        isafter = (day < anotherDate.day) ? false : isafter;

        return isafter;
    }

    public int dayDifference(MyDate anotherDate) {
        if(this.isAfter(anotherDate)){
            int dayDiff = this.day+ (maxDays[anotherDate.month-1] - anotherDate.day)+(this.month-anotherDate.month-1)*30;
            return dayDiff;
        }
        else {
            int dayDiff = anotherDate.day+ (maxDays[this.month-1] - this.day)+(anotherDate.month-this.month-1)*30;
            return dayDiff;
        }
    }
}