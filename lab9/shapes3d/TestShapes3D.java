package shapes3d;

import shapes2d.Square;

public class TestShapes3D {
    public static void main(String[] args){

        Square s = new Square(5);

        Cube c = new Cube(5);

        System.out.println("c.equals(s) : "+c.equals(s));
        System.out.println("s.equals(c) : "+s.equals(c));

        Cylinder cy = new Cylinder(4,7);
        System.out.println(cy.area());
        System.out.println(cy.volume());
        System.out.println(cy);



    }
}
